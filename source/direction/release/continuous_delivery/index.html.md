---
layout: markdown_page
title: "Category Vision - Continuous Delivery"
---

- TOC
{:toc}

## Continuous Delivery

Many teams are reporting that development velocity is stuck; they've reached a plateau in moving to more and more releases per month, and need help on how to improve. At the moment, 40% of software development team's top priorities relate to speed/automation (source: Forrester Research). Our overriding vision for Continuous Delivery is to help these teams renew their ability to accelerate.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=continuous%20delivery&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) / 
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask 
questions about what's coming.

### Continuous Integration vs. Delivery vs. Deployment

We follow the well-known [definitions](https://martinfowler.com/bliki/ContinuousDelivery.html) from Martin Fowler on the difference between continuous integration, continuous delivery, and continuous deployment. To quote important parts of his article:

- **Continuous Integration** usually refers to integrating, building, and testing code within the development environment. Continuous Delivery builds on this, dealing with the final stages required for production deployment.
- **Continuous Delivery** is a software development discipline where you build software in such a way that the software can be released to production at any time. You achieve continuous delivery by continuously integrating the software done by the development team, building executables, and running automated tests on those executables to detect problems. Furthermore you push the executables into increasingly production-like environments to ensure the software will work in production.
- **Continuous Deployment** means that every change goes through the pipeline and automatically gets put into production, resulting in many production deployments every day. Continuous Delivery just means that you are able to do frequent deployments but may choose not to do it, usually due to businesses preferring a slower rate of deployment. In order to do Continuous Deployment you must be doing Continuous Delivery.

_Source: https://martinfowler.com/bliki/ContinuousDelivery.html_

### Incremental Rollout

One of the key sub-areas within Continuous Delivery that we're tracking is incremental rollout. This is important because it gives unprecedented control over how you deliver your software in a safe way.  For these items we're also tagging the label "incremental rollout", and you can see an issue list of these items [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=incremental%20rollout&sort=milestone).

## What's Next & Why

[gitlab-ee#7380](https://gitlab.com/gitlab-org/gitlab-ee/issues/7380) is next up for CD, which builds the foundation needed to deliver merge trains as a complete feature. Following up on this we will look at [gitlab-ee#9186](https://gitlab.com/gitlab-org/gitlab-ee/issues/9186) as a way to solve contention issues that large teams or teams using monorepos will run into. With that, we've really delivered on the promise of merge trains.

## Competitive Landscape

### Azure DevOps

Microsoft is planning to use their GitHub acquisition to pick up market share in going after open source software development for their new integrated solution. There had been a trend to move away from them because users saw their current development tools as old-fashioned; with rebranding them to Azure Devops (and investment in improving their capabilities) they are trying to disrupt this decision by their customers.

Microsoft has always been strong in providing actionable metrics on development, and as they move forward to integrate GitHub and Azure DevOps they will be able to provide a wealth of new metrics to help teams using their solution. We can stay in front of this by improving our own actionable delivery metrics in the product via [gitlab-ee#7838](https://gitlab.com/gitlab-org/gitlab-ee/issues/7838).

### Spinnaker & Harness

Spinnaker and Harness are two modern, cloud-native CD platforms that provide excellent solutions for delivering to cloud environments using native approaches like Kubernetes. In order to remain competitive, we must provide more advanced deployment operator solutions via [gitlab-ee#1387](https://gitlab.com/gitlab-org/gitlab-ee/issues/1387). Additionally, these products manage the deployment all the way through to monitoring, which we will introduce via [gitlab-ee#8295](https://gitlab.com/gitlab-org/gitlab-ee/issues/8295).

## Analyst Landscape

In talking with analysts at Forrester, there are a number of key trends occurring in the CD space:

### Cloud Adoption
Cloud adoption of CI/CD is growing, with Docker adoption leading the way and serverless likely next. People need guidance solving CD because they are stepping out into the dark without mature solutions already available to them; for example, AWS' current solution for serverless (functions) CI is just "edit them live." Customers complained that their products were starting to feel legacy, but where they go next is unclear.

### Customer Experience
Customer experience is becoming a key metric. Users are looking for the ability to not just  measure platform stability and other performance KPIs post-deployment but also want to set targets for customer behavior, experience, and financial impact. Tracking and measuring this indicators after deployment solves an important pain point. In a similar fashion, creating views which managing products not projects or repos will provide users with a more relevant set of data.

### Access Controls & Security
People want tools that isolate areas they should have access to; you shouldn't have to have admin access in order to get your job done. Also, things like the CI/CD tool should not have root everywhere. Container security, secrets management, API gateways; people need help with navigating these solutions. Furthermore, providing authentication of artifacts from build to release (chain of custody) is valuable.

### Analyst Evaluation Criteria
In addition to supporting the trends above, there are some key areas where we can focus that will improve our solution in areas that analysts are hearing customer demand:

- **Modeling (and autodiscovery/templatizing) of environments and deployments** as-code, and in a way that they can automatically interact with each other to generate deployment plans without requiring scripting. Views that make these complex relationships between environments and deployments clear.
- **Deployment status reporting, monitoring behavior, and error handling**, including automated rollbacks, incremental rollouts, and other intelligent built-in and customizable strategies and behaviors.
- **Configuration drift detection** between what's in our internal model of what's deployed, vs. what's actually in the environments with alerting and potential automatic remediation.
- Take control of your software delivery by **assembling associated changes into merge trains** which can flow through your environments and on to production as a coherent group.
- **Advanced, predictive analytics/ML and reporting about successes, failures, and pipeline health**, both retrospective and runtime. Customizable and tailored for the user/role engaging with the data.

Much of what analysts are looking for are evolutions in the [release governance](/direction/release/release_governance) space. That said, within this area analytics for improvement [gitlab-ce#52882](https://gitlab.com/gitlab-org/gitlab-ce/issues/52882) can make a big difference.

## Top Customer Success/Sales Issue(s)

The ability to control pipeline concurrency to an environment (especially later ones such as production or staging) is frequently mentioned by CS and in the sales cycle as a feature teams are looking for. This will be implemented via [gitlab-ce#20481](https://gitlab.com/gitlab-org/gitlab-ce/issues/20481).

## Top Customer Issue(s)

Our most popular customer request is [gitlab-ce#20481](https://gitlab.com/gitlab-org/gitlab-ce/issues/20481), which limits per-environment pipeline concurrency. This is an important feature because it allows teams to constrain concurrent deployments, making things more predictible and/or managing finite resources associated with environments (for example, a single deploy and test cycle at a time to a performance testing environment.)

## Top Internal Customer Issue(s)

Solving merge trains via [gitlab-ee#7380](https://gitlab.com/gitlab-org/gitlab-ee/issues/7380) and [gitlab-ee#9186](https://gitlab.com/gitlab-org/gitlab-ee/issues/9186) will allow our own engineering teams to be more effective while maintaining a stable, releasable master branch.

## Top Vision Item(s)

[gitlab-ee#7380](https://gitlab.com/gitlab-org/gitlab-ee/issues/7380) is part of our Product Vision 2018, our most popular customer request, and a further step towards delivering merge trains.

Beyond completing the 2018 vision, the most important major step forward for GitLab CD is [gitlab-ee#8295](https://gitlab.com/gitlab-org/gitlab-ee/issues/8295) which will introduce post-deployment monitoring, creating a foundation for advanced deployment features like automated rollbacks and summary reports of environment behavior before and after the deployment.

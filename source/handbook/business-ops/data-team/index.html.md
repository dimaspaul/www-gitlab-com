---
layout: markdown_page
title: "Data Team"
description: "GitLab Data Team Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

{::options parse_block_html="true" /}

----

## <i class="fab fa-gitlab fa-fw icon-color font-awesome" aria-hidden="true"></i> Quick Links
[Primary Project](https://gitlab.com/gitlab-data/analytics/){:.btn .btn-purple-inv}

[Looker Project](https://gitlab.com/gitlab-data/looker/){:.btn .btn-purple-inv}

[dbt docs](https://gitlab-data.gitlab.io/analytics/dbt/snowflake/#!/overview){:.btn .btn-purple-inv}

## <i class="fas fa-map-signs fa-fw icon-color font-awesome" aria-hidden="true"></i> Roadmap

[Epics](https://gitlab.com/groups/gitlab-data/-/roadmap?layout=MONTHS&sort=start_date_asc){:.btn .btn-purple}

[OKRs](https://about.gitlab.com/company/okrs/){:.btn .btn-purple}

----

<br />

<div class="alert alert-purple center">
  <p class="purple center" style="font-size: 34px ; text-align: center ; margin: auto">
    <strong>We <i class="fab fa-gitlab orange font-awesome" aria-hidden="true"></i> Data</strong>
  </p>
</div>
{:.no_toc}

## <i class="fas fa-chart-line fa-fw icon-color font-awesome" aria-hidden="true"></i> Data Analysis Process

Analysis usually begins with a question. A stakeholder will ask a question of the data team by creating an issue in the [Data Team project](https://gitlab.com/gitlab-data/analytics/) using the appropriate template. The analyst assigned to the project may schedule a discussion with the stakeholder(s) to further understand the needs of the analysis. This meeting will allow for analysts to understand the overall goals of the analysis, not just the singular question being asked, and should be recorded. Analysts looking for some place to start the discussion can start by asking: 
* How can your favorite reports be improved?
* How do you use this data to make decisions?
* What decisions do you make and what information will help you to make them quicker/better?

An analyst will then update the issue to reflect their understanding of the project at hand. This may mean turning an existing issue into a meta issue or an epic. Stakeholders are encouraged to engage on the appropriate issues. The issue then becomes the SSOT for the status of the project, indicating the milestone to which its been assigned and the analyst working on it, among other things. Barring any confidentiality concerns, the issue is also where the final project will be delivered. On delivery, the data team  manager will be cc'ed where s/he will provide feedback and/or request changes. When satisfied, s/he will close the issue. If the stakeholder would like to request a change after the issue has been closed, s/he should create a new issue and link to the closed issue. 

The Data Team can be found in the #analytics channel on slack.

## <i class="fas fa-tasks fa-fw icon-color font-awesome" aria-hidden="true"></i> Getting Things Done

The data team currently works in two-week intervals, called milestones. Milestones start on Tuesdays and end on Mondays. This discourages last-minute merging on Fridays and allows the team to have milestone planning meetings at the top of the milestone.

Milestones may be three weeks long if they cover a major holiday or if the majority of the team is on vacation. As work is assigned to a person and a milestone, it gets a weight assigned to it. 

### Issue Pointing

* We use Fibonacci numbers for pointing, e.g. 1, 2, 3, 5, 8, etc. 
* If something is greater than 8 points, it probably should be multiple issues. 
* We size and point issues as a group. Size is about the complexity of the problem and not its complexity in relation to whom is expected to complete the task.
* Effective pointing requires more fleshed out issues, but that requirement shouldn't keep people from creating issues.
* When pointing work that happens outside of the Analytics or Looker repos, add points to the issue in our repos.
* Rules of thumb:
   * `NULL` points belongs to a meta issue, where the sub issues get points.
   * 0 points is a not-complex unit of work and is different from `NULL`.
   * If we start working on an issue that we decide to stop working on, the issue keeps its point value. Just use a label to indicate that work is not being done on it.

### Merge Request Workflow

*Ideally*, your workflow should be as follows:
1. Create an issue
1. Open an MR from the issue using the "Create merge request" button. This automatically creates a unique branch based on the issue name. This marks the issue for closure once the MR is merged.
1. Push your work to the branch
1. Run any relevant jobs to the work being proposed
  * e.g. if you're working on dbt changes, run the dbt MR job and the dbt test job.
1. Document in the MR description what the purpose of the MR is, any additional changes that need to happen for the MR to be valid, and if it's a complicated MR, how you verified that the change works. See [this MR](https://gitlab.com/gitlab-data/analytics/merge_requests/658) for an example of good documentation. The goal is to make it easier for reviewers to understand what the MR is doing so it's as easy as possible to review.
1. Assign the MR to a peer to have it reviewed. If assigning to someone who can merge, either leave a comment asking for a review without merge, or you can simply leave the `WIP:` label.
1. Once it's ready for further review and merging, remove the `WIP:` label, mark the branch for deletion, mark squash commits, and assign to the project's maintainer.

Other tips:
* If, for some reason, the merge request is closed but not Merged, you have to run the review stop job manually. Closing the MR will not trigger it. 
* If you're on a review instance of the database and you need to test a change to the SFDC snapshot, truncate the table first.


## <i class="fas fa-exchange-alt fa-fw icon-color font-awesome" aria-hidden="true"></i> Extract and Load

We currently use Stitch for most of our data sources. In some cases, we use [Meltano](/handbook/meltano/).

| Data Source       | Pipeline  | Management Responsibility                                                                                                                                              |
|-------------------|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| CloudSQL Postgres | Stitch    | Data Team                                                                                                                                                              |
| Gitter            |           |                                                                                                                                                                        |
| SheetLoad         | SheetLoad | Data Team                                                                                                                                                              |
| Marketo           | Stitch    | Data Team                                                                                                                                                              |
| Netsuite          | Stitch    | Data Team                                                                                                                                                              |
| Pings             | Meltano   | The Meltano team will investigate the source of the problem.  If the issue is in the Meltano tap, the Meltano team will fix it.  If not, the data team is responsible. |
| SFDC              | Stitch    | Data Team                                                                                                                                                              |
| Snowplow          |           | Data Team                                                                                                                                                              |
| Zendesk           | Stitch    | Data Team                                                                                                                                                              |
| Zuora             | Stitch    | Data Team                                                                                                                                                              |

### Adding new Data Sources

Process for adding a new data source:
* Create a new issue in the Analytics repo requesting for the data source to be added:
  * Document what tables and fields are required
  * Document the questions that this data will help answer
* Create an issue in the [Security project](https://gitlab.com/gitlab-com/gl-security/engineering/issues/) and cross-link to the Analytics issue.
  * Tag the Security team `gitlab-com/gl-security`


### Using SheetLoad

SheetLoad is the process by which a GoogleSheet, local CSV or file from GCS can be ingested into the data warehouse. This is not an ideal solution to get data into the warehouse, but may be the appropriate solution at times. 

As it is being iterated on often, the best place for up-to-date info on Sheetload is the [Sheetload readme](https://gitlab.com/gitlab-data/analytics/tree/master/extract/sheetload).






## <i class="fas fa-clock fa-fw icon-color font-awesome" aria-hidden="true"></i> Orchestration

We are in the process of moving from GitLab CI to Airflow.

## <i class="fas fa-database fa-fw icon-color font-awesome" aria-hidden="true"></i> Data Warehouse

We currently use [Snowflake](https://docs.snowflake.net/manuals/index.html) as our data warehouse.

### Warehouse Access

To gain access to the data warehouse: 
* Create an issue in the [access requests project](https://gitlab.com/gitlab-com/access-requests) documenting the level of access required.
* Do not request a shared account - each account must be tied to a user.

<div class="panel panel-success">
**Managing Roles for Snowflake**
{: .panel-heading}
<div class="panel-body">

Here are the proper steps for provisioning a new user and user role:

* Login and switch to `SECURITYADMIN` role
* Create user (`EBURKE`)
* Create a password using https://passwordsgenerator.net/
* Click next and fill in additional info. 
  * Make Login Name and Display name match user name (all caps).
  * Do not set any defaults.
  * Send to person using https://onetimesecret.com/
* Create role for user (`EBURKE` for example) with `SYSADMIN` as the parent role (this grants the role to sysadmin)
* Grant user role to new user
* Create user_scratch schema in `ANALYTICS` as `SYSADMIN`
  * `CREATE SCHEMA eburke_scratch;`
* Grant ownership of scratch schema to user role
  * `GRANT OWNERSHIP ON schema eburke_scratch TO ROLE eburke;`
* Document in Snowflake config.yml permissions file
</div>
</div>


## <i class="fas fa-cogs fa-fw icon-color font-awesome" aria-hidden="true"></i> Transformation

* Getting started with dbt
* How to use dbt docs

### Tips and Tricks about Working with dbt
* The goal of a (final) `_xf` dbt model should be a `BEAM*` table, which means it follows the business event analysis & model structure and answer the who, what, where, when, how many, why, and how question combinations that measure the business. 
* Model names should be as obvious as possible and should use full words where possible, e.g. `accounts` instead of `accts`.
* Documenting and testing new data models is a part of the process of creating them. A new dbt model is not complete without tests and documentation.  
* Definitions to know
   * `source table` - (can also be called `raw table`) table coming directly from data source as configured by the manifest. It is stored directly in a schema that indicates its original data source, e.g. `sfdc`
   * `base models`- the only dbt models that reference the source table; base models have minimal transformational logic (usually limited to filtering out rows with data integrity issues or actively flagged not for analysis and renaming columns for easier analysis); can be found in `analytics` schema; is used in `ref` statements by `end-user models`
   * `end-user models` - dbt models used for analysis. The final version of a model will likely be indicated with an `_xf` suffix when it’s goal is to be a `BEAM*` table. It should follow the business event analysis & model structure and answer the who, what, where, when, how many, why, and how question combinations that measure the business.


## <i class="fas fa-chart-bar fa-fw icon-color font-awesome" aria-hidden="true"></i> Visualization

Looker is GitLab's data visualization tool. Many modern data visualization tools require analysts to write SQL queries; Looker's unique advantage lies in their modeling layer that allows non-technical end users to build their own data analysis in a drag and drop user interface. While this means that the initial configuration (e.g., setting up a new data source) takes longer than just querying a table would be, once the initial configuration is done, you have a new data set- a Looker explore- available for all users to take advantage of. The data team aims for Looker to be the SSOT for all of GitLab. 

### Getting Looker Access
To get initial Looker Access, please create a new access issue following Security's procedures in [this project](https://gitlab.com/gitlab-com/access-requests). There are multiple levels of user access: View-Only, Explorer, and Developer. 
All GitLab Employees should be a part of their respective team group as well as the `GitLab Employee` group. If you do not have access, please see the handbook on how to request it.

### User Roles

We have 4 role types: View Only, Explorer, Developer, Admin.

* View Only - Users can only view looks/dashboards and update any filters visible on those
* Explorer - Users can leverage explores (data sets) to create new looks and dashboards using the provided explores
* Developer - Users can edit the LookML in the user interface and make MR's to the project (technically any user can make an MR to the repo itself outside of Looker)
* Admin - Can manager users, roles, groups, and all looks/dashboards.

### Getting Started with Looker- A special note for users coming from Redash
Users coming from Redash or another query-based data visualization tool, especially those with a strong familiarity with SQL, may find themselves uniquely frustrated at how long it can take to answer a "simple" question when doing so aims to take advantage of new data. Any new data sources need to be brought into the data warehouse, modeled in dbt following the team's [dbt coding conventions](https://gitlab.com/gitlab-data/analytics/blob/master/README.md#dbt), modeled in a LookML view, and added to a new or existing explore before an analysis can be built on top of it. While this initial configuration might seem like a bit of a *slog*, it moved all of the analyses configuration to be an up-front responsibility, making the explore you build usable not just for you, but for future users with related questions too. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/GwrgbSOfcH4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Core Looker Definitions & Structure

**LookML** - is a Looker proprietary language for describing dimensions, aggregates, calculations and data relationships in a SQL database. The Looker app uses a model written in LookML to construct SQL queries against a particular database.

**LookML Project** - A LookML project is a collection of LookML files that describe a set of related models, explores, views, and LookML dashboards.

By convention, LookML code is segregated into three types of files: model files, view files, and dashboard files. In new LookML, these files have the following extensions: .model.lkml, .view.lkml, and .dashboard.lookml, respectively.

**Model** - A model is a customized portal into the database, designed to provide intuitive data exploration for specific business users. Multiple models can exist for the same database connection in a single LookML project. Each model can expose different data to different users. For example, sales agents need different data than company executives, and so you would probably develop two models to offer views of the database appropriate for each user. A model, in turn, is essentially metadata about the source database.

**View** - A view declaration defines a list of fields (dimensions or measures) and their linkage to an underlying table or derived table. In LookML a view typically references an underlying database table, but it can also represent a derived table.

A view may join to other views. The relationship between views is typically defined as part of a explore declaration in a model file.

**Explore** - An explore is a view that users can query. You can think of the explore as a starting point for a query, or in SQL terms, as the FROM in a SQL statement. An explore declaration specifies the join relationships to other views.

**Derived Table** - A derived table is a table comprised of values from other tables, which is accessed as though it were a physical table with its own set of columns. A derived table is exposed as its own view using the derived_table parameter, and defines dimensions and measures in the same manner as conventional views. Users can think of a derived table similar to a **view**, not to be confused with a Looker view, in a database.

**Dimension Group** - The dimension_group parameter is used to create a set of time-based dimensions all at once. For example, you could easily create a date, week, and month dimension based on a single timestamp column.

* Using Looker
* Developing in Looker
Many times it can be easier to develop in the Looker UI, so that you can actually make changes to charts/dashboards/explores, etc. from the same branch. Those changes get pushed to prod as part of your branch.
Recommended process:
   1. Make change to measure
   1. Look at explore and use measure
   1. Save measure to dashboard
   1. Push branch
   1. Gets merged
   1. Dashboard includes new metric
This way you deliver everything in one package.

### Tips, Tricks, and Best Practices about Working in Looker
* Confused about what you might be looking at? Start by understanding our [Operating Metrics](/handbook/finance/operating-metrics/).
* When making a change to an existing `_xf` view, be sure to limit explores already referencing that view with sets, so as not to accidentally clutter existing explores with new/irrelevant data. 
* If building a dashboard with historical data, it should be for historical data. It should not include current data. For example, a dashboard that looks at the last year, should not include the present month. 
* When adding a description to a value that has both a description and an abbreviation, be sure to include both in the description, e.g. `Technical Account Manager` should be `Technical Account Manager (TAM)`. This is to make it easier for users to search for fields within Looker and avoids ambiguity.
* Make mulitple small explores intead of one big one.
* Design each explore to answer a specific set of questions. Single-view explores are fine!
* 5 to 6 view joins should be the limit. Otherwise, the underlying data may need to be modeled more effectively.
* Only include specific fields that answer the question needed for analysis. Do not include fields that won't be used.
* Don't use full outer joins.
* Don’t use many-to-many joins.
* Default colors are `#4b4ba3, #7c7ccc, #e05842, #fca121, #2e87e0, #37b96d, #707070, #bababa, #494C5` based on the GitLab [style guide](https://design.gitlab.com/styles/colors).
* Max of 2 decimal places for numbers.
* `$#,##0` is an appropriate format for most dollar values.
* Each dashboard should only be focused on one time frame and a date filter should apply to every visualization equally. 
* A visualization looking at historical data, for example by month, should not include the present month (because it's not a comparison of apples to apples). 
* Allow the consumer of a visualization to do some work but not so much that it would dissuade them from consuming the visualization. Reduce clutter/labels where their presence will be redundant (e.g. where the Y axis already has numbers, it may be redundant to also label it "Count of X").
* Definitions should be established and then applied to dashboard/looks/explores equally everywhere. Definitions should be done at the upper-most level, so that business-logic changes in the future do not need to be maintained in multiple places.
* Comparisons of bookings values should be against Plan (Company-approved plan), Target (Goal based on industry benchmarks), or Forecast (Approved forecast, if different from Plan.)
* When developing, check the content validator to confirm you haven't broken any existing dashboards and/or visualizations. If you have, remap them, communicate with the stake holder, or create an issue to remedy them. 
* Percentages should appear as integer values, e.g. `30%`
* Chart Titles should exist to explain the chart

## <i class="fas fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i> Team Roles

### Data Analyst
[Position Description](/job-families/finance/data-analyst/){:.btn .btn-purple}

### Data Engineer
[Position Description](/job-families/finance/data-engineer/){:.btn .btn-purple}

### Manager
[Position Description](/job-families/finance/manager-data-and-analytics/){:.btn .btn-purple}


<!-- EXTRA STYLES APPLIED FOR THIS PAGE ONLY -->

<style>
.purple {
  color: rgb(107,79,187) !important;
}
.orange {
  color:rgb(252,109,38) !important;
}
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
.btn-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-purple:hover {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv:hover {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-orange {
  color: rgb(252,109,38);
  background-color: #fff;
  border-color: rgb(226,67,41);
}
.btn-orange:hover {
  color: #fff;
  background-color: rgb(252,109,38);
  border-color: rgb(226,67,41);
}
.product.thumbnail img {
  display: block;
  max-width: 50%;
  margin: 20px auto;
}
.thumbnail img {
  display: block;
  max-width: 30%;
  margin: 20px auto;
}
.caption h4 {
  text-align: center;
}
.mkt-box {
  padding-bottom: 10px;
  padding-top: 10px;
  cursor: pointer;
}
.mkt-box:hover {
  /*border-radius: 5px;*/
  box-shadow:0 1px 5px rgba(0,0,0,0.3), 0 0 2px rgba(0,0,0,0.1) inset;
}
.mkt-row {
  padding-top: 20px;
  padding-bottom: 5px;
}
.mkt-row a:focus {
  outline: none;
}
.modal-header h2 {
  margin-top: 0;
}
.modal-footer p {
  margin-bottom: 0;
}
.center {
  text-align: center;
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.description {
  color: #999;
}
.extra-space {
  margin-bottom: 5px;
}
.alert-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: rgba(107,79,187,.5);
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
ul.toc-list-icons li ul li i.slack {
  color: rgb(224,23,101);
}
ul.toc-list-icons li ul li i.email {
  color: rgb(192,0,0);
}
</style>


---
layout: markdown_page
title: "Technical Writing"
---

### Welcome to the Technical Writing Handbook!
{:.no_toc}

## On this page
{:.no_toc}

* Will be replaced with the ToC, excluding the "On this page" header
{:toc}

## Technical Writing

The Technical Writing team is responsible for:

* [docs.gitlab.com](https://gitlab.com/gitlab-com/gitlab-docs)
* CE docs
* EE docs
* Written technical tutorials which are in CE or EE docs
  (video tutorials will come later).

At GitLab, technical writers help ensure that GitLab's documentation remains complete,
correct, and easy to use. They partner with anyone in the GitLab community who is
concerned with docs, especially developers, who are the first to update docs for
the GitLab features that they code.

We are there to assist developers in their documentation writing process by
providing copy editing and reviewing services and are not responsible for
writing the first draft of documentation for new or updated features.

We maintain and improve the overall health of documentation
by creating topic index pages, improving organization, creating tutorials, etc.

We manage our documentation tasks for CE and EE on the following issues boards
which track labels beginning with `docs-`:

* [CE Documentation Issue Board](https://gitlab.com/gitlab-org/gitlab-ce/boards/106589)
* [EE Documentation Issue Board](https://gitlab.com/gitlab-org/gitlab-ee/boards/266349)

For more information on documentation at GitLab, see the [Documentation](../../documentation/) section of the Handbook.

The Technical Writing team is part of the Product team and has the following responsibilities:

- **Documentation Content**
    - Reviewing and collaborating on documentation plans, reviewing doc merge requests or recently merged docs, and ensuring that content meets style and language standards.
    - Reorganizing, revamping, and authoring content to ensure completeness and a smooth user experience.
    - Fostering the development of new content to meet the needs of the community.

- **Documentation Site** (docs.gitlab.com)
    - Maintaining and enhancing the documentation site’s architecture, design, automation, versioning, search, SEO, feedback methods, and analytics.

- **Documentation Process**
    - Ensuring that processes are in place and being followed to keep the GitLab docs up to date.
    - Following and optimizing documentation workflows with Product and Engineering, Documentation team workflows, and the division of work.
    - Triaging doc-related issues.
    - Refining the Documentation Style Guide and continuously improving content about GitLab documentation and its contribution process.
    - Making it easier for anyone to contribute to the documentation while efficiently handling community contributions to docs.

- **Collaboration**
    - Working on documentation efforts with Product, Support, Marketing, Engineering, Community Marketing, other GitLab teams, and the wider GitLab community.
    - Ensuring that relevant documentation is easily accessible from within the product.
    - Acting as reviewers of the monthly [release post](../../handbook/marketing/blog/release-posts/).

## Documentation standards and workflows

For information on creating documentation at GitLab, see our [Documentation](../documentation/) handbook page and our
[documentation on how to contribute](https://docs.gitlab.com/ee/development/documentation).

require 'spec_helper'

describe 'the salary calculator', :js do
  before do
    visit '/handbook/people-operations/global-compensation/calculator/'
    select_calculator_field('role', 'Backend Engineer')
  end

  def select_calculator_field(field, value)
    find(".salary-container .#{field} .dropdown-toggle").click

    if %w[country area].include?(field)
      find(".salary-container .#{field} [type='search']").fill_in(with: value)
    end

    find(".salary-container .#{field} .key", text: value).click
  end

  context 'when the position is in a country with a currency on our currencies list' do
    before do
      select_calculator_field('country', 'Netherlands')
      select_calculator_field('area', 'Everywhere else')
    end

    let!(:original_salary) { find('.compensation .amount').text }
    let!(:original_salary_eur) { find('.compensation .converted-local-currency').text }

    it 'shows the salary in USD and in the local currency' do
      expect(find('.compensation .amount')).to have_text(/\$\d+,\d+ - \$\d+,\d+/)
      expect(find('.compensation .converted-local-currency')).to have_text(/\d+,\d+ EUR - \d+,\d+ EUR/)
    end

    it 'changes salary when level changes' do
      select_calculator_field('level', 'Senior')

      expect(find('.compensation .amount')).not_to have_text(original_salary)
      expect(find('.compensation .converted-local-currency')).not_to have_text(original_salary_eur)
    end

    it 'changes salary when experience factor changes' do
      select_calculator_field('experience', 'Learning the role')

      expect(find('.compensation .amount')).not_to have_text(original_salary)
      expect(find('.compensation .converted-local-currency')).not_to have_text(original_salary_eur)
    end

    it 'changes salary when area changes' do
      select_calculator_field('area', 'Amsterdam')

      expect(find('.compensation .amount')).not_to have_text(original_salary)
      expect(find('.compensation .converted-local-currency')).not_to have_text(original_salary_eur)
    end

    it 'resets area when country changes' do
      select_calculator_field('country', 'Italy')

      expect(page).not_to have_css('.area .title')
      expect(find('.compensation .amount')).not_to have_text(original_salary)
      expect(page).not_to have_css('.compensation .converted-local-currency')
    end
  end

  context 'when the position is in a country without a currency on our currencies list' do
    before do
      select_calculator_field('country', 'Switzerland')
      select_calculator_field('area', 'Everywhere else')
    end

    it 'shows the salary in USD only' do
      expect(find('.compensation .amount')).to have_text(/\$\d+,\d+ - \$\d+,\d+/)
      expect(page).not_to have_css('.compensation .converted-local-currency')
    end
  end

  context 'when the location is on the do-not-hire list' do
    before do
      select_calculator_field('country', 'Spain')
    end

    it 'shows a message' do
      expect(page).to have_css('.js-country-no-hire')
    end
  end
end
